use std::io::prelude::*;
use std::sync::atomic::{AtomicBool, AtomicU32, Ordering};
use std::sync::mpsc::{sync_channel, Receiver, SyncSender};
use std::sync::Arc;
use std::time::Duration;

use clap::Parser;
use ffmpeg::{
	format::{context::Input, input, Pixel},
	media::Type,
	software::scaling::{context::Context, flag::Flags},
	threading::Config,
	util::frame::video::Video,
};
use rayon::prelude::*;
use tiff::encoder::{colortype, TiffEncoder};

type SumPixel = u32;

#[derive(Parser)]
#[command(author, about, long_about = None)]
struct Opts {
	/// The number of threads to use for decoding.
	#[clap(long, default_value = "8")]
	decode_threads: usize,

	/// Makes the program not output anything.
	#[clap(long, short)]
	quiet: bool,

	/// The input video (anything supported by FFmpeg).
	input_file: String,

	/// The output image (TIFF only).
	#[clap(default_value = "out.tif")]
	output_file: String,
}

fn main() {
	let opts = Opts::parse();

	ffmpeg::init().unwrap();

	let ictx = input(opts.input_file).expect("Failed to open input file.");
	let input = ictx
		.streams()
		.best(Type::Video)
		.ok_or(ffmpeg::Error::StreamNotFound)
		.unwrap();

	let video_stream_index = input.index();

	let codec_parameters = input.parameters();
	let codec_id = codec_parameters.id();
	let codec = ffmpeg::codec::decoder::find(codec_id).expect("Failed to find codec");

	let mut decoder = ffmpeg::codec::Context::new_from(codec).decoder();
	decoder
		.set_parameters(codec_parameters)
		.expect("Failed to set codec parameters");

	decoder.set_threading(Config {
		kind: ffmpeg::threading::Type::Frame,
		count: opts.decode_threads,
		safe: true,
	});

	let video = decoder.video().expect("Failed to open video decoder");

	let width = video.width();
	let height = video.height();

	let scaler = Context::get(
		video.format(),
		width,
		height,
		Pixel::RGB24,
		width,
		height,
		Flags::BILINEAR,
	)
	.expect("Failed to create swscale context");

	let (tx, rx) = sync_channel(10);
	let mut reader = Reader::new(video, scaler, tx);
	let mut summer = Summer::new(width as usize, height as usize, rx);

	std::thread::scope(|scope| {
		let run = Arc::new(AtomicBool::new(true));
		let num_decoded_frames = reader.num_decoded_frames.clone();
		let num_processed_frames = summer.num_processed_frames.clone();

		scope.spawn(|| {
			reader.decode(video_stream_index, ictx).unwrap();
		});

		if !opts.quiet {
			let run_clone = run.clone();
			scope.spawn(move || {
				while run_clone.load(Ordering::Relaxed) {
					print!(
						"\rdecoded: {}, processed: {}",
						num_decoded_frames.load(Ordering::Relaxed),
						num_processed_frames.load(Ordering::Relaxed)
					);
					std::io::stdout().flush().unwrap();
					std::thread::sleep(Duration::from_millis(100));
				}
			});
		}

		summer.process();
		run.store(false, Ordering::Relaxed);
	});

	let mut file = std::fs::OpenOptions::new()
		.truncate(true)
		.write(true)
		.create(true)
		.open(opts.output_file)
		.expect("couldn't open output file");

	let mut tiff_enc = TiffEncoder::new(&mut file).expect("failed to create tiff");
	let tiff_img = tiff_enc
		.new_image::<colortype::RGB32Float>(width, height)
		.unwrap();

	// tiff_img
	// 	.encoder()
	// 	.write_tag(Tag::Artist, "Image-tiff")
	// 	.unwrap();

	let total_frames = summer.num_processed_frames.load(Ordering::Relaxed) as f32;
	let floats = summer
		.sum
		.iter()
		.map(|x| *x as f32 / total_frames / 255.)
		.collect::<Vec<_>>();

	tiff_img.write_data(floats.as_slice()).unwrap();
	//tiff_img.finish().unwrap(); // ?
}

struct Reader {
	num_decoded_frames: Arc<AtomicU32>,
	video: ffmpeg::decoder::Video,
	scaler: Context,
	tx: SyncSender<Option<Video>>,
}

impl Reader {
	pub fn new(
		video: ffmpeg::decoder::Video,
		scaler: Context,
		tx: SyncSender<Option<Video>>,
	) -> Self {
		Self {
			num_decoded_frames: Arc::new(AtomicU32::new(0)),
			video,
			scaler,
			tx,
		}
	}

	fn decode(&mut self, video_stream_index: usize, mut ictx: Input) -> Result<(), ffmpeg::Error> {
		for res in ictx.packets() {
			let (stream, packet) = res?;
			if stream.index() == video_stream_index {
				self.video.send_packet(&packet)?;
				self.receive_and_process_decoded_frames()?;

				// // DEBUG
				// if total_frames >= 100 {
				// 	break;
				// }
			}
		}
		self.video.send_eof()?;
		self.receive_and_process_decoded_frames()?;

		self.tx.send(None).unwrap();

		Ok(())
	}

	fn receive_and_process_decoded_frames(&mut self) -> Result<(), ffmpeg::Error> {
		let mut decoded = Video::empty();

		while self.video.receive_frame(&mut decoded).is_ok() {
			let mut rgb_frame = Video::empty();
			self.scaler.run(&decoded, &mut rgb_frame)?;
			//self.process_frame(&rgb_frame)?;
			self.tx.send(Some(rgb_frame)).unwrap();
			self.num_decoded_frames.fetch_add(1, Ordering::Relaxed);
		}
		Ok(())
	}
}

struct Summer {
	width: usize,
	sum: Vec<SumPixel>,
	rx: Receiver<Option<Video>>,
	num_processed_frames: Arc<AtomicU32>,
}

impl Summer {
	pub fn new(width: usize, height: usize, rx: Receiver<Option<Video>>) -> Self {
		let sum: Vec<SumPixel> = vec![SumPixel::default(); 3 * width * height];
		Self {
			width,
			sum,
			rx,
			num_processed_frames: Arc::new(AtomicU32::new(0)),
		}
	}

	pub fn process(&mut self) {
		for frame in self.rx.iter() {
			match frame {
				Some(frame) => {
					let data = frame.data(0);

					// https://rust.godbolt.org/z/chYPfK5nb

					data.par_chunks_exact(3 * self.width)
						.zip(self.sum.par_chunks_exact_mut(3 * self.width))
						.for_each(|(source_line, dest_line)| {
							for (source, dest) in source_line.iter().zip(dest_line.iter_mut()) {
								*dest += *source as SumPixel;
							}
						});

					self.num_processed_frames.fetch_add(1, Ordering::Relaxed);
				}
				None => {
					break;
				}
			}
		}
	}
}
