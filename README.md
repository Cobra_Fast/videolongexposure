# videolongexposure

Condenses a video into one long exposure photograph

## Motivation

Traditionally daytime long exposure photography involved the use of a neutral density (ND) filter to cut down the amount of light hitting the lens and sensor to manageable levels when exposing for extended periods of time. Obviously the same can be achieved by shortening exposure time. Naturally, adding up enough normal exposures will eventually equal the same amount of total exposure time that would have been achievable with an ND filter.

The caveats of this multi exposure long exposure technique are twofold: firstly there are noticeable time gaps between exposures that may show up in the added result, secondly it puts severalfold the amount of wear and tear on the shutter (and mirror) mechanisms than a single exposure would.

## Solution

Leveraging the high resolution video capabilites of modern digital cameras allows us to combine all the advantages: Using the digital shutter minimizes time gaps between exposure if a frame rate close to the exposure time is chosen and it avoids nearly all wear on the mechanical parts of the camera, while 4K image resolution (that's 8.3 MP) is good enough for most digital media and prints.

This simple console program implements the practical software post-processing side necessary to execute this solution. All pixels of all frames are added up into a single image buffer and, as a final step, divided by the total number of frames seen. This guarantees each original video frame has the same amount of sway onto the final image. If enough cumulative exposure time was collected in the video, the end result is virtually indistinguishable from a single long-time exposure.

Furthermore the program is able to leverage multiple CPU cores/threads for both, video decoding and data condensation.

## Usage

For the real world recording stage I strongly recommend the use of a tripod and full manual exposure settings, so that nothing about the perspective or individual frames' exposure changes throughout the video recorded.

Afterwards condensing the video is as simple as `./videolongexposure video.mp4 longexposure.tiff`.

The amount of decoding CPU threads used can be set with the `--decode-threads` option; it defaults to `8`.

The produced TIFF image contains 32 bit floating point data, providing the most amount of data for further post-processing with your photo editor of choice.

### Memory consumption

Overall memory consumption is typically between 300 and 500 MB, the main contributors are:

* Up to 10 raw 24bpp video frames will be buffered in memory at a time, at 2160p that may amount to 25 MB.
* One 192bpp image buffer all video frames are added onto, at 2160p that's almost 200 MB.
* Whatever else FFmpeg needs to decode the video, which will vary based on the codec and its configuration in the video.

## Thanks

to nitroxis (<https://nxs.re/>, <https://gitlab.com/nitroxis>) for providing all of the Rust know-how.

### Libraries used

* FFmpeg/libav <https://ffmpeg.org/>
* rayon <https://github.com/rayon-rs/rayon>
* tiff <https://github.com/image-rs/image-tiff>
* clap <https://github.com/clap-rs/clap>
